package testng;

	import org.openqa.selenium.WebElement;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.DataProvider;
	import org.testng.annotations.Test;

	import wdMethods.ProjectMethods;

	public class TC002_CreateLead extends ProjectMethods{

	@ BeforeTest
		public void setdata() {
			testCaseName="TC002_CreateLead";
			testCaseDesc="Create lead";
			category="smoke";
			author="suryanaji";
			excelfilename="Createlead";	
		}

		@Test(dataProvider ="fetchdata")
		public void createLead(String cname,String fname, String lname) {
			WebElement leadclick = locateElement("xpath","//a[text()='Leads']");
			click(leadclick);
			WebElement click = locateElement("linktext","Create Lead");
			click(click);
			WebElement cmpyname = locateElement("id","createLeadForm_companyName");
			type(cmpyname, cname);
			WebElement firstname = locateElement("id","createLeadForm_firstName");
			type(firstname,fname);
			WebElement lastname = locateElement("id","createLeadForm_lastName");
			type(lastname,lname);
			WebElement firstlocalame = locateElement("id","createLeadForm_firstNameLocal");
			type(firstlocalame,"najba");
			WebElement lastnamelocal = locateElement("id","createLeadForm_lastNameLocal");
			type(lastnamelocal,"shahidha");
			WebElement salutation = locateElement("id","createLeadForm_personalTitle");
			type(salutation,"qa");
		WebElement dropdown = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(dropdown,"Conference");
		WebElement title = locateElement("id","createLeadForm_generalProfTitle");
		type(title,"ssss");
		WebElement annualrevenue =locateElement("id","createLeadForm_annualRevenue");
		type(annualrevenue,"12333" );
		WebElement industry = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingText(industry,"Computer Software");
		WebElement ownership = locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownership,"Corporation");
		WebElement siccode = locateElement("id","createLeadForm_sicCode");
		type(siccode,"64528");
		WebElement desc = locateElement("id","createLeadForm_description");
		type(desc,"meetings");
		WebElement impnote = locateElement("id","createLeadForm_importantNote");
		type(impnote,"emergency");
		WebElement countrycode = locateElement("id","createLeadForm_primaryPhoneCountryCode");
		type(countrycode,"1000");
		WebElement areacode = locateElement("id","createLeadForm_primaryPhoneAreaCode");
		type(areacode,"96");
		WebElement ext = locateElement("id","createLeadForm_primaryPhoneExtension");
		type(ext,"044");
		WebElement dep = locateElement("id","createLeadForm_primaryPhoneExtension");
		type(dep,"AA");
		WebElement currency = locateElement("id","createLeadForm_currencyUomId");
		selectDropDownUsingText(currency,"PLZ - Poland");
		WebElement emp = locateElement("id","createLeadForm_numberEmployees");
		type(emp,"500");
		WebElement ticketsym = locateElement("id","createLeadForm_tickerSymbol");
		type(ticketsym,"u162");
		WebElement ask = locateElement("id","createLeadForm_primaryPhoneAskForName");
		type(ask,"naji");
		WebElement url = locateElement("id","createLeadForm_primaryWebUrl");
		type(url,"www.urjanet.com");
		WebElement toname = locateElement("id","createLeadForm_generalToName");
		type(toname,"anu");
		WebElement address = locateElement("id","createLeadForm_generalAddress1");
		type(address,"Nehru st");
		WebElement city = locateElement("id","createLeadForm_generalCity");
		type(city,"chennai");
		WebElement state = locateElement("id","createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(state,"South Carolina");
		WebElement country = locateElement("id","createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(country,"American Samoa");
		WebElement zip= locateElement("id","createLeadForm_generalPostalCode");
		type(zip,"600094");
		WebElement zipext = locateElement("id","createLeadForm_generalPostalCodeExt");
		type(zipext,"94");
		WebElement markcamp = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(markcamp,"Affiliate Sites");
		WebElement phone= locateElement("id","createLeadForm_primaryPhoneNumber");
		type(phone,"236846565");
		WebElement email= locateElement("id","createLeadForm_primaryEmail");
		type(email,"suryakalas17@gmail.com");
		WebElement click1 = locateElement("class","smallSubmit");
		click(click1);
		}
		
				
		
				





	}




