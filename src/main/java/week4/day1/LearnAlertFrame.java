lpackage week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrame {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
	    driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
	    driver.switchTo().frame("iframeResult");
	    driver.findElementByXPath("//button[text()='Try it']").click();
	    driver.switchTo().alert().sendKeys("suryakala");
	    Thread.sleep(3000);
	    driver.switchTo().alert().accept();
	    String text = driver.findElementById("demo").getText();
	    if(text.contains("suryakala")) {
	    	System.out.println("correct");
	    }
	    	else {
	    		System.out.println("incorrect");
	    	}
	    }
	}
	


