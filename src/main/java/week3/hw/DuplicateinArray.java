package week3.hw;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class DuplicateinArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = {13,65,15,88,65,13,99,67,13,65,87,13,67};
		Set<Integer> dupNumbers = new LinkedHashSet<Integer>();
		for(int i=0;i<a.length;i++) {
			for(int j=i+1;j<a.length;j++) {
				if(a[j]==a[i]) {
					dupNumbers.add(a[i]);
				}
			}
		}
		System.out.println("Duplicate number in array:" +dupNumbers);
		
		}
		

	}


