package week3.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Checkbox {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
         driver.get("http://testleaf.herokuapp.com/");
	   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElementByLinkText("Checkbox").click();
        WebElement checkbox=driver.findElementByXPath("//*[@id='contentblock']/section/div[1]/input[1]");
        checkbox.click();
        if (checkbox.isSelected()) 
			System.out.println("The check box is selected");
		else
			System.out.println("The check box is not selected");
		//driver.close();
		
		}		
	}


