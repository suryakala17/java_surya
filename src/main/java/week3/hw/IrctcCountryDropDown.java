package week3.hw;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcCountryDropDown {

	public static void main(String[] args) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
			    driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
			    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                WebElement sc=driver.findElementById("userRegistrationForm:nationalityId");
				Select dropdown=new Select(sc);
                List<WebElement> options=dropdown.getOptions();
				for (WebElement eachoption : options) {
					if (eachoption.getText().startsWith("E")) {
						int i =0;
						i++;
						if (i==2) {
							System.out.println(eachoption.getText());
							
							eachoption.click();
							break;
						}
					}
				}

				//close browser
				driver.close();
			}

		}


	


