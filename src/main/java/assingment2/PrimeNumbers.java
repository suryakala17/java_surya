package assingment2;

public class PrimeNumbers {

	public static void main(String[] args) {
		 //define limit
        int limit = 20;

        System.out.println(" prime numbers betewen 1 and 20");

        //loop through the numbers one by one
        for(int i=1; i < 20; i++){

                boolean isPrime = true;

                //check to see if the number is prime
                for(int j=2; j < i ; j++){

                        if(i % j == 0){
                                isPrime = false;
                                break;
                        }
                }
                // print the number
                if(isPrime)
                        System.out.print(i + " ");
        }
}
	}

