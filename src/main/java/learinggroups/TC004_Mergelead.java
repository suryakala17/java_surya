package learinggroups;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004_Mergelead extends ProjectMethods{
	@Test
	
	public void mergelead() throws InterruptedException{
			driver.findElementByLinkText("Leads").click();
			Thread.sleep(3000);
			driver.findElementByLinkText("Merge Leads").click();
			driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
			Set<String> allwindows = driver.getWindowHandles();
			List<String> listofwindows = new ArrayList<String>();
			listofwindows.addAll(allwindows);
			driver.switchTo().window(listofwindows.get(1));
			driver.manage().window().maximize();
			driver.findElementByXPath("//span[text()='Find by'");
			driver.findElementByXPath("//span[text()='Name and ID'").click();
			
			driver.findElementByXPath("//input[@name='id']").sendKeys("10218");
			Thread.sleep(4000);
	        driver.findElementByXPath("//button[text()='Find Leads']").click();
	        Thread.sleep(5000);
	         WebElement actualdata1 = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
	         String actualdatatext1= actualdata1.getText();
	         System.out.println(actualdatatext1);
	         String expecteddatatext1 ="10835";
	         if(actualdatatext1.contains(expecteddatatext1)) {
				System.out.println("Pass");
			}else {
				System.out.println("Fail");
			}
			 driver.switchTo().window(listofwindows.get(0));
			 driver.findElementByXPath("//a[text()='Merge']").click();
			 Alert mergealert = driver.switchTo().alert();
			 String actualmessage = mergealert.getText();
	          String expectedmessage="Are you sure?";
	             if(actualmessage.contains(expectedmessage)) {
					System.out.println("Pass");
				}else {
					System.out.println("Fail");
				}
				mergealert.accept();
				/*WebElement actualerrormessage = driver.findElementByXPath("//div[@class='messages']");
	             String actualerrortext = actualerrormessage.getText();
	             System.out.println("Error message is" + actualerrortext);
	             String expectederrormessage="The Following Errors Occurred:";
	            if( actualerrortext.contains(expectederrormessage)) {
					System.out.println("Pass");
				}else {
					System.out.println("Fail");
				}
				
				//driver.navigate().back();
				driver.close();
				//driver.quit();*/


	}

}
