package week1hw;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		System.out.println("Enter a number:"); 
		int n,rem,r;   //r-reverse number
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		r = 0;
		while(n > 0)
		{
			rem = n % 10;
			r = (r*10)+rem;
			n = n/10;
		}	
		System.out.println("Reverse number is:" + r );
		}

}