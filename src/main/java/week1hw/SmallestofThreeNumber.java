package week1hw;

import java.util.Arrays;
import java.util.Scanner;

public class SmallestofThreeNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter number of elements in array:"); 
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int a[] = new int[n];
		System.out.println("Enter elements of array:");
		for (int i=0;i<n;i++) {
		a[i] = sc.nextInt();
		
		}
		Arrays.sort(a);
		System.out.println("The second smallest number is:" + a[1]);
	}

}
