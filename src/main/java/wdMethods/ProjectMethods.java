package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import worksheet.CreateLeadWorkSheet;

public class ProjectMethods extends SeMethods{
public String filename;	
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass
	public void beforeClass() {
		startTestCase();
	}
	
	
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url,String password,String username) {
		startApp("chrome",url);		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,username );
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement crmfclick = locateElement("linktext","CRM/SFA");
		click(crmfclick);
	}
	
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name = "fetchdata")
	public Object[][]fetchdata() throws IOException{
    return CreateLeadWorkSheet.getExcelData(filename);	
	}
	
	
	
	
	
	
}
