package testcase;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
// testcase level
		ExtentTest test = extent.createTest("TC001_LoginAndLogOut", "Login into the page");
		test.assignCategory("smoke");
		test.assignAuthor("suryanaji");
		//teststeplevel
	test.pass("Browser launched",
	MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	test.pass("username entered succrssfully",
	MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
	test.pass("password entered succrssfully",
	MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
	extent.flush();
	
	
	}

}
