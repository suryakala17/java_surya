package testcase;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethodsold.SeMethods;

public class TC_003EditLead extends ProjectMethods  {
	@Test(dependsOnMethods= {"testcase.TC002_CreateLead.createLead"})
	public void editLead() {
		
		
		WebElement leadsclick = locateElement("linktext","Leads");
		click(leadsclick);
		WebElement findleads = locateElement("linktext","Find Leads");
		click(findleads);
		WebElement firstname =locateElement("xpath","(//input[@class=' x-form-text x-form-field '])[1]");
		type(firstname,"babu");
		WebElement findclick = locateElement("linktext","Find Leads");
		click(findclick);
		WebElement firstlead = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]");
		click(firstlead);
		
		
		

	}
}
