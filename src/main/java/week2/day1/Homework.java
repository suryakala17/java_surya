package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Homework {
	
	

	//Program to print in the sorted order of the input AmazonIndia
	//print character in reverse sorted order without case sensitive
	//remove duplicate and print in sorted order


	
		public static void main(String[] args) 
		{
			String name ="AmazonIndia";
			List<Character> eachChar = new ArrayList<Character>();
			char []allChar=name.toCharArray();
			
			for (char c : allChar) 
			{
				eachChar.add(c);
			}
			
			Collections.sort(eachChar);
			System.out.println("printing the sorted order");
			for (char sort : eachChar) 
			{
				System.out.println(sort);
			}
			eachChar.clear();
			String uCase=name.toUpperCase();
			char []uChar=uCase.toCharArray();
			
			for (char d : uChar) 
			{
				eachChar.add(d);
			}
			Collections.sort(eachChar);
			Collections.reverse(eachChar);
			System.out.println("printing the sorted reverse order");
			for (char rev : eachChar) 
			{
				System.out.println(rev);
			}
			//to remove duplicate
			Set<Character> dup = new HashSet<>();
			dup.addAll(eachChar);
			eachChar.clear();
			eachChar.addAll(dup);
			Collections.sort(eachChar);
			System.out.println("Removed duplicates and printing in sorted order");
			for (Character g : eachChar)
			{
				System.out.println(g);
			}
		}

	}



