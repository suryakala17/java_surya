package worksheet;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateLeadWorkSheet {

	public static Object[][] getExcelData(String filename) throws IOException {
		// TODO Auto-generated method stub
       XSSFWorkbook wbook = new XSSFWorkbook("./data/CreateLead.xlsx");
       XSSFSheet sheet = wbook.getSheetAt(0);
       int RowNum = sheet.getLastRowNum();
       System.out.println("Row Count = "+RowNum);
       int CellNum = sheet.getRow(0).getLastCellNum();
       System.out.println("column Count ="+CellNum );
       Object[][] data = new Object[RowNum][CellNum];
       for (int j=1;j <=RowNum;j++) {
    	 XSSFRow row = sheet.getRow(j);
    	 for (int i=0;i< CellNum;i++) {
       XSSFCell cell = row.getCell(i);
       data[j-1][i]= cell.getStringCellValue();
       
       }
       }
      
       wbook.close();
       return data;
	}

}
