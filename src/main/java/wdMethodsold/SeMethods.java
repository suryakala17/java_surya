package wdMethodsold;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver  = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" launched successfully");
		}
		catch(WebDriverException e) {//webdriver:any driver is missing
			System.out.println("WebDriverException has Occured");
			throw new RuntimeException("WebDriverException has Occured");
		}catch(NullPointerException e1){
			System.out.println("nullPointerExcpetion has Occured");
			throw new RuntimeException("NullPointerException has Occured");//after finding the error it should be stop test case
		}catch(Exception e2) {//it will throw the any error
			System.out.println("Expetion has Occured");
			throw new RuntimeException("Exception has Occured");
		}
		finally {
			takeSnap();
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);	
			case "partiallinktext":return driver.findElementByPartialLinkText(locValue);
			case "name":return driver.findElementByName(locValue);
			case "cssselector":return driver.findElementByCssSelector(locValue);
			case "tagname":return driver.findElementByTagName(locValue);
			}
			//return null;
		}


		catch(NoSuchElementException e) {
			System.out.println("NoSuchElement Exception has Occured");
			throw new RuntimeException("NoSuchElement Exception has Occured");
		}catch(ElementNotVisibleException e1) {
			System.out.println("ElementnotvisibleException has Occured");
			throw new RuntimeException("ElementnotvisibleException has Occured");
		}catch(StaleElementReferenceException e2) {
			System.out.println("StaleElement Reference Exception has Occured");
		}

		catch(Exception e2) {
			System.out.println("Expetion has Occured");
			throw new RuntimeException("Exception has Occured");
		}


		return null;


	}


	public WebElement locateElement(String locValue) {	
		try {
			return driver.findElementById(locValue);
		}
		catch(NoSuchElementException e) {
			System.out.println("No Such element exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e2) {
			System.out.println("Expetion has Occured");
			throw new RuntimeException("Exception has Occured");
		}
		//return null;
	}

	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The Data "+data+" is entered Successfully");

		}catch(NoSuchElementException e) {
			System.out.println("No Such element exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");

		}
		catch(Exception e2) {
			System.out.println("Expetion has Occured");
			throw new RuntimeException("Exception has Occured");
		}
		finally {
			takeSnap();
		}
	}


	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" is clicked Successfully");
		}catch(NoSuchElementException e) {
			System.out.println("No Such element exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e2) {
			System.out.println("Expetion has Occured");
			throw new RuntimeException("Exception has Occured");
		}
		finally {
			takeSnap();
		}
	}


	public String getText(WebElement ele) {
		try {
			String text = ele.getText();

			System.out.println("The Element text is "+text);
			return text;
		}

		catch(NoSuchElementException e) {
			System.out.println("Nosuchelement exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
		//return null;

	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select select1=new Select(ele);
		select1.selectByVisibleText(value);

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select select2=new Select(ele);
		select2.selectByIndex(index);

	}
	public void selectDropDownUsingValue(WebElement ele, String value) {
		Select select3=new Select(ele);
		select3.selectByValue(value);


	}
	public void selectDropDownUsingText_1(WebElement ele,String selectMethod,String value) {

		Select dd=new Select(ele);
		try {
			if(selectMethod.equalsIgnoreCase("visible")) {
				dd.selectByVisibleText(value);
			}else if(selectMethod.equalsIgnoreCase("value")){

				dd.selectByValue(value);
			}else if(selectMethod.equalsIgnoreCase("index")) {
				dd.selectByIndex(Integer.parseInt(value));
			}
			System.out.println("The dropdwon is selected with text"+value);
		}catch(WebDriverException e) {
			System.out.println("The element :"+ele+"could not found");
			throw new RuntimeException("Webdriverexception has Occured");
		}catch(Exception e1) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {

		try {
			String actualtitle = driver.getTitle();
			if(actualtitle.contains(expectedTitle)) {
				System.out.println("Title is Verified");
				return true;
			}else {
				System.out.println("Title does not match");
			}
			//return false;
		}catch(Exception e1) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String actualText = ele.getText();

			if(actualText.equals(expectedText)) {
				System.out.println("Correct Text");
			}else {
				System.out.println("Wrong Text");
			}
		}
		catch(NoSuchElementException e) {
			System.out.println("Nosuchelement exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e1) {
			System.out.println("exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
		//return null;


	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String pactualText = ele.getText();

			if(pactualText.contains(expectedText)) {
				System.out.println("Correct Text");
			}else {
				System.out.println("Wrong Text");
			}
		}catch(NoSuchElementException e) {
			System.out.println("Nosuchelement exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e1) {
			System.out.println("exception has occured");
			throw new RuntimeException("Exception has Occured");
		}


	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String actualattribute = ele.getAttribute(attribute);


			if(actualattribute.equals(value)) {
				System.out.println("Correct value");
			}else
			{
				System.out.println("Wrong value");
			}
		}
		catch(NoSuchElementException e) {
			System.out.println("Nosuchelement exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e1) {
			System.out.println("exception has occured");
			throw new RuntimeException("Exception has Occured");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String pactualattribute = ele.getAttribute(attribute);


			if(pactualattribute.contains(value)) {
				System.out.println("Correct value");
			}else
			{
				System.out.println("Wrong value");
			}
		}catch(NoSuchElementException e) {
			System.out.println("Nosuchelement exception has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e1) {
			System.out.println("exception has occured");
			throw new RuntimeException("Exception has Occured");
		}



	}

	@Override
	public void verifySelected(WebElement ele) {

		// boolean selected = ele.isSelected();
		try {
			if(ele.isSelected()) {
				System.out.println("Element is Selected");

			}else {
				System.out.println("Element is Not Selected");
			}
		}catch(ElementNotSelectableException e) {
			System.out.println("ElementNotSelectableException has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}catch(Exception e1) {
			System.out.println(" exception has occured");
			throw new RuntimeException("Exception has Occured");
		}



		//System.out.println(selected);

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		/* boolean displayed = ele.isDisplayed();

		System.out.println(displayed);*/

		try {
			if(ele.isDisplayed()) {
				System.out.println("Element is Displayed");

			}else {
				System.out.println("Element is not Displayed");
			}
		}

		catch(Exception e) {
			System.out.println(" exception has occured");
			throw new RuntimeException("Exception has Occured");
		}


	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowHandles = driver.getWindowHandles();

			ArrayList<String> listofwindoows = new ArrayList<String>();

			listofwindoows.addAll(windowHandles);

			driver.switchTo().window(listofwindoows.get(index));
		}
		catch(NoSuchWindowException e) {
			System.out.println("NoSuchWindowException has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}catch(Exception e1) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);

		}catch(NoSuchFrameException e) {
			System.out.println("NoSuchFrameException has occured");
			throw new RuntimeException("NoSuchFrameException has Occured");
		}catch(NoSuchElementException e1) {
			System.out.println("NoSuchElementException e1");
			throw new RuntimeException("NoSuchElementException has Occured");

		}catch(Exception e2) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
	}


	public void switchToFrameIndex(int index) {
		try {
			driver.switchTo().frame(index);
		}catch(NoSuchFrameException e) {
			System.out.println("NoSuchFrameException has occured");
			throw new RuntimeException("NoSuchFrameException has Occured");
		}catch(NoSuchElementException e1) {
			System.out.println("NoSuchElementException e1");
			throw new RuntimeException("NoSuchElementException has Occured");
		}catch(Exception e2) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
	}


	public void switchToFrameString(String data) {
		try {
			driver.switchTo().frame(data);
		}catch(NoSuchFrameException e) {
			System.out.println("NoSuchFrameException has occured");
			throw new RuntimeException("NoSuchFrameException has Occured");
		}catch(NoSuchElementException e1) {
			System.out.println("NoSuchElementException e1");
			throw new RuntimeException("NoSuchElementException has Occured");
		}catch(Exception e2) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
	}






	@Override
	public void acceptAlert() {

		try {
			driver.switchTo().alert().accept();
		}catch(NoAlertPresentException e) {
			System.out.println("NoAlertPresentException has occured");
			throw new RuntimeException("NoAlertPresentException has Occured");
		}catch(UnhandledAlertException e1) {
			System.out.println("UnhandledAlertException has occured");
			throw new RuntimeException("UnhandledAlertException has Occured");

		}catch(Exception e2) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}


	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
		}
		catch(NoAlertPresentException e) {
			System.out.println("NoAlertPresentException has occured");
			throw new RuntimeException("NoAlertPresentException has Occured");
		}catch(UnhandledAlertException e1) {
			System.out.println("UnhandledAlertException has occured");
			throw new RuntimeException("NUnhandledAlertException has Occured");

		}catch(Exception e2) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}

	}

	@Override
	public String getAlertText() {
		try {
			String alerttext = driver.switchTo().alert().getText();
			return alerttext;
		}catch(NoSuchElementException e) {
			System.out.println("NoSuchElementException has occured");
			throw new RuntimeException("NoSuchElementException has Occured");
		}
		catch(Exception e1) {
			System.out.println("Exception has occured");
			throw new RuntimeException("Exception has Occured");
		}
		//return null;
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");		
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {

		driver.quit();


	}
}


